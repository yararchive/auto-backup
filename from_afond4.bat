@echo off
cls

rem �����⮢�� �����
set BACKUPPATH=\\winserver2008\e$\mailru_cloud\_backups
set AF5GAYOPATH=%BACKUPPATH%\af5_gayo
set AF4GAYOPATH=%BACKUPPATH%\af4_gayo
set AF5CDNIPATH=%BACKUPPATH%\af5_cdni
set AF4CDNIPATH=%BACKUPPATH%\af4_cdni

mkdir "%AF5GAYOPATH%"
mkdir "%AF4GAYOPATH%"
mkdir "%AF5CDNIPATH%"
mkdir "%AF4CDNIPATH%"

start /wait net use "\\winserver2008\e$" /user:USER PASSWORD /PERSISTENT:No

set ARCHPATH=C:\Program Files (x86)\7-Zip\7z.exe

rem �������� 䠩��� 7z ���� 30 ����
rem FORFILES /S /P "%BACKUPPATH%" /M *.7z /D -30 /C "cmd /c del @file"

echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=gayo%DNAME%
set BNAME=af5_gayo_%DNAME%.bak
set FULL_BPATH=%AF5GAYOPATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_LNAME=C:\_backups\%BNAME%
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� ��5 ���� � %BNAME%
sqlcmd -E -S "(local)\sqlexpress" -d master -Q "BACKUP DATABASE [af5_gayo] TO DISK = N'%FULL_LNAME%' WITH NOUNLOAD, NAME = N'AF5 GAYO backup', NOSKIP , STATS = 10, NOFORMAT" -o "%FULL_LNAME%.log"
"%ARCHPATH%" -mx9 a "%FULL_LNAME%.7z" "%FULL_LNAME%" "%FULL_LNAME%.log" -p%PNAME%
echo f | xcopy /Z /Y /H "%FULL_LNAME%.7z" "%FULL_BNAME%.7z"
del /F /Q "%FULL_LNAME%"
del /F /Q "%FULL_LNAME%.log"
del /F /Q "%FULL_LNAME%.7z"
echo ����ࢭ�� ����� ᮧ����
echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=cdni%DNAME%
set BNAME=af5_cdni_%DNAME%.bak
set FULL_BPATH=%AF5CDNIPATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_LNAME=C:\_backups\%BNAME%
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� ��5 ���� � %BNAME%
sqlcmd -E -S "(local)\sqlexpress" -d master -Q "BACKUP DATABASE [af5_cdni] TO DISK = N'%FULL_LNAME%' WITH NOUNLOAD, NAME = N'AF5 CDNI backup', NOSKIP , STATS = 10, NOFORMAT" -o "%FULL_LNAME%.log"
"%ARCHPATH%" -mx9 a "%FULL_LNAME%.7z" "%FULL_LNAME%" "%FULL_LNAME%.log" -p%PNAME%
echo f | xcopy /Z /Y /H "%FULL_LNAME%.7z" "%FULL_BNAME%.7z"
del /F /Q "%FULL_LNAME%"
del /F /Q "%FULL_LNAME%.log"
del /F /Q "%FULL_LNAME%.7z"
echo ����ࢭ�� ����� ᮧ����
echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=gayo%DNAME%
set BNAME=af4_gayo_%DNAME%.bak
set FULL_BPATH=%AF4GAYOPATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_LNAME=C:\_backups\%BNAME%
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� ��4 ���� � %BNAME%
sqlcmd -E -S "(local)\sqlexpress" -d master -Q "BACKUP DATABASE [AF4_DB] TO DISK = N'%FULL_LNAME%' WITH NOUNLOAD, NAME = N'AF4 GAYO backup', NOSKIP , STATS = 10, NOFORMAT" -o "%FULL_LNAME%.log"
"%ARCHPATH%" -mx9 a "%FULL_LNAME%.7z" "%FULL_LNAME%" "%FULL_LNAME%.log" -p%PNAME%
echo f | xcopy /Z /Y /H "%FULL_LNAME%.7z" "%FULL_BNAME%.7z"
del /F /Q "%FULL_LNAME%"
del /F /Q "%FULL_LNAME%.log"
del /F /Q "%FULL_LNAME%.7z"
echo ����ࢭ�� ����� ᮧ����

echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=cdni%DNAME%
set BNAME=af4_cdni_%DNAME%.bak
set FULL_BPATH=%AF4CDNIPATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_LNAME=C:\_backups\%BNAME%
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� ��4 ���� � %BNAME%
sqlcmd -E -S "(local)\sqlexpress" -d master -Q "BACKUP DATABASE [CDNI_DB] TO DISK = N'%FULL_LNAME%' WITH NOUNLOAD, NAME = N'AF4 CDNI backup', NOSKIP , STATS = 10, NOFORMAT" -o "%FULL_LNAME%.log"
"%ARCHPATH%" -mx9 a "%FULL_LNAME%.7z" "%FULL_LNAME%" "%FULL_LNAME%.log" -p%PNAME%
echo f | xcopy /Z /Y /H "%FULL_LNAME%.7z" "%FULL_BNAME%.7z"
del /F /Q "%FULL_LNAME%"
del /F /Q "%FULL_LNAME%.log"
del /F /Q "%FULL_LNAME%.7z"
echo ����ࢭ�� ����� ᮧ����
echo ============================================
