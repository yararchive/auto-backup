@echo off
cls

rem �����⮢�� �����
set BACKUP_PATH=E:\mailru_cloud\_backups
set MIRROR_PATH=\\filer001\backup
set AF5_PAR2014_PATH=%BACKUP_PATH%\af5_2graph_2014
set AF5_PAR_PATH=%BACKUP_PATH%\af5_paragraph
set INQ_CDNI_PATH=%BACKUP_PATH%\inquiry_cdni
set INQ_GAYO_PATH=%BACKUP_PATH%\inquiry_gayo
set INQ_ANSWERS_CDNI_PATH=%BACKUP_PATH%\inquiry_answers_cdni
set INQ_ANSWERS_GAYO_PATH=%BACKUP_PATH%\inquiry_answers_gayo
set CHURCH_GAYO_PATH=%BACKUP_PATH%\church_gayo
set SBIS_CDNI_PATH=%BACKUP_PATH%\sbis_cdni
set SBIS_GAYO_PATH=%BACKUP_PATH%\sbis_gayo

mkdir "%AF5_PAR2014_PATH%"
mkdir "%AF5_PAR_PATH%"
mkdir "%INQ_CDNI_PATH%"
mkdir "%INQ_GAYO_PATH%"
mkdir "%CHURCH_GAYO_PATH%"
mkdir "%INQ_ANSWERS_CDNI_PATH%"
mkdir "%INQ_ANSWERS_GAYO_PATH%"
mkdir "%SBIS_CDNI_PATH%"
mkdir "%SBIS_GAYO_PATH%"

set ARCHPATH=C:\Program Files\7-Zip\7z.exe

rem �������� 䠩��� 7z ���� 30 ����
rem FORFILES /S /P "%BACKUP_PATH%" /M *.7z /D -30 /C "cmd /c del @file"

echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=gayo%DNAME%
set BNAME=sbis_gayo_%DNAME%
set FULL_BPATH=%SBIS_GAYO_PATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� ���� ���� � %BNAME%
start /wait net use "\\server" /user:servercdni\USER PASSWORD /PERSISTENT:No
echo d | xcopy /Z /Y /H /E "\\server\SBIS" "%SBIS_GAYO_PATH%\SBIS"
"%ARCHPATH%" -mx9 a "%FULL_BNAME%.7z" "%SBIS_GAYO_PATH%\SBIS" -p%PNAME%
RMDIR /S /Q "%SBIS_GAYO_PATH%\SBIS"
echo ����ࢭ�� ����� ᮧ����
echo �������� ����� १�ࢭ�� �����
FORFILES /S /P "%SBIS_GAYO_PATH%" /M *.7z /D -30 /C "cmd /c del @file"
echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=cdni%DNAME%
set BNAME=sbis_cdni_%DNAME%
set FULL_BPATH=%SBIS_CDNI_PATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� ���� ���� � %BNAME%
start /wait net use "\\servercdni" /user:servercdni\USER PASSWORD /PERSISTENT:No
echo d | xcopy /Z /Y /H /E "\\servercdni\SBIS" "%SBIS_CDNI_PATH%\SBIS"
"%ARCHPATH%" -mx9 a "%FULL_BNAME%.7z" "%SBIS_CDNI_PATH%\SBIS" -p%PNAME%
RMDIR /S /Q "%SBIS_CDNI_PATH%\SBIS"
echo ����ࢭ�� ����� ᮧ����
echo �������� ����� १�ࢭ�� �����
FORFILES /S /P "%SBIS_CDNI_PATH%" /M *.7z /D -30 /C "cmd /c del @file"
echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=cdni%DNAME%
set BNAME=inquiry_answers_cdni_%DNAME%
set FULL_BPATH=%INQ_ANSWERS_CDNI_PATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �⢥⮢ �� ���� � %BNAME%
start /wait net use "\\servercdni" /user:servercdni\USER PASSWORD /PERSISTENT:No
echo d | xcopy /Z /Y /H /E "\\servercdni\���� ��饭��\���饭�� �ࠦ���\�⢥��" "%INQ_ANSWERS_CDNI_PATH%\�⢥��"
"%ARCHPATH%" -mx9 a "%FULL_BNAME%.7z" "%INQ_ANSWERS_CDNI_PATH%\�⢥��" -p%PNAME%
RMDIR /S /Q "%INQ_ANSWERS_CDNI_PATH%\�⢥��"
echo ����ࢭ�� ����� ᮧ����
echo �������� ����� १�ࢭ�� �����
FORFILES /S /P "%INQ_ANSWERS_CDNI_PATH%" /M *.7z /D -30 /C "cmd /c del @file"
echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=gayo%DNAME%
set BNAME=inquiry_answers_gayo_%DNAME%
set FULL_BPATH=%INQ_ANSWERS_GAYO_PATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �⢥⮢ �� ���� � %BNAME%
start /wait net use "\\server" /user:server\USER PASSWORD /PERSISTENT:No
echo d | xcopy /Z /Y /H /E "\\server\���� ���饭��\���饭�� �ࠦ���\�⢥��" "%INQ_ANSWERS_GAYO_PATH%\�⢥��"
"%ARCHPATH%" -mx9 a "%FULL_BNAME%.7z" "%INQ_ANSWERS_GAYO_PATH%\�⢥��" -p%PNAME%
RMDIR /S /Q "%INQ_ANSWERS_GAYO_PATH%\�⢥��"
echo ����ࢭ�� ����� ᮧ����
echo �������� ����� १�ࢭ�� �����
FORFILES /S /P "%INQ_ANSWERS_GAYO_PATH%" /M *.7z /D -30 /C "cmd /c del @file"
echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=gayo%DNAME%
set BNAME=af5_paragraph_%DNAME%.bak
set FULL_BPATH=%AF5_PAR_PATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� ��5 ��ࠣ�� 2013 � %BNAME%
sqlcmd -E -S "(local)" -d master -Q "BACKUP DATABASE [ArchiveFund5] TO DISK = N'%FULL_BNAME%' WITH NOUNLOAD, NAME = N'AF5 2graph 2013 backup', NOSKIP , STATS = 10, NOFORMAT" -o "%FULL_BNAME%.log"
"%ARCHPATH%" -mx9 a "%FULL_BNAME%.7z" "%FULL_BNAME%" "%FULL_BNAME%.log" -p%PNAME%
del /F /Q "%FULL_BNAME%"
del /F /Q "%FULL_BNAME%.log"
echo ����ࢭ�� ����� ᮧ����
echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=gayo%DNAME%
set BNAME=af5_2graph_2014_%DNAME%.bak
set FULL_BPATH=%AF5_PAR2014_PATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� ��5 ��ࠣ�� 2014 � %BNAME%
sqlcmd -E -S "(local)" -d master -Q "BACKUP DATABASE [af5_2graph_2014] TO DISK = N'%FULL_BNAME%' WITH NOUNLOAD, NAME = N'AF5 2graph 2014 backup', NOSKIP , STATS = 10, NOFORMAT" -o "%FULL_BNAME%.log"
"%ARCHPATH%" -mx9 a "%FULL_BNAME%.7z" "%FULL_BNAME%" "%FULL_BNAME%.log" -p%PNAME%
del /F /Q "%FULL_BNAME%"
del /F /Q "%FULL_BNAME%.log"
echo ����ࢭ�� ����� ᮧ����
echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=cdni%DNAME%
set BNAME=inquiry_cdni_%DNAME%
set FULL_BPATH=%INQ_CDNI_PATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� �� ���� � %BNAME%
start /wait net use "\\servercdni" /user:servercdni\USER PASSWORD /PERSISTENT:No
echo f | xcopy /Z /Y /H "\\servercdni\���� ��饭��\���饭�� �ࠦ���\Inquiry.sdb" "%INQ_CDNI_PATH%\Inquiry.sdb"
"%ARCHPATH%" -mx9 a "%FULL_BNAME%.7z" "%INQ_CDNI_PATH%\Inquiry.sdb" -p%PNAME%
del /F /Q "%INQ_CDNI_PATH%\Inquiry.sdb"
del /F /Q /AH "%INQ_CDNI_PATH%\Inquiry.sdb"
echo ����ࢭ�� ����� ᮧ����
echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=gayo%DNAME%
set BNAME=inquiry_gayo_%DNAME%
set FULL_BPATH=%INQ_GAYO_PATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� �� ���� � %BNAME%
start /wait net use "\\server" /user:server\USER PASSWORD /PERSISTENT:No
echo f | xcopy /Z /Y /H "\\server\���� ���饭��\���饭�� �ࠦ���\Inquiry.sdb" "%INQ_GAYO_PATH%\Inquiry.sdb"
"%ARCHPATH%" -mx9 a "%FULL_BNAME%.7z" "%INQ_GAYO_PATH%\Inquiry.sdb" -p%PNAME%
del /F /Q "%INQ_GAYO_PATH%\Inquiry.sdb"
del /F /Q /AH "%INQ_GAYO_PATH%\Inquiry.sdb"
echo ����ࢭ�� ����� ᮧ����
echo ============================================
set BYEAR=%date:~6,4%
set BMONTH=%date:~3,2%
set BDAY=%date:~0,2%
set YYYYMMDD=%date:~6,4%%date:~3,2%%date:~0,2%
if "%time:~0,1%"==" " (set HH=0%time:~1,1%) else (set HH=%time:~0,2%)
set MM=%time:~3,2%
set SS=%time:~6,2%
set DNAME=%YYYYMMDD%%HH:~-2,2%%MM:~-2,2%%SS:~-2,2%
set PNAME=gayo%DNAME%
set BNAME=church_gayo_%DNAME%
set FULL_BPATH=%CHURCH_GAYO_PATH%\%BYEAR%\%BMONTH%\%BDAY%
mkdir "%FULL_BPATH%"
set FULL_BNAME=%FULL_BPATH%\%BNAME%
echo ����஢���� �� ���ਪ ���� � %BNAME%
start /wait net use "\\server" /user:server\USER PASSWORD /PERSISTENT:No
echo f | xcopy /Z /Y /H "\\server\��\Church.mdb" "%CHURCH_GAYO_PATH%\Church.mdb"
"%ARCHPATH%" -mx9 a "%FULL_BNAME%.7z" "%CHURCH_GAYO_PATH%\Church.mdb" -p%PNAME%
del /F /Q "%CHURCH_GAYO_PATH%\Church.mdb"
del /F /Q /AH "%CHURCH_GAYO_PATH%\Church.mdb"
echo ����ࢭ�� ����� ᮧ����
echo ============================================
robocopy "%BACKUP_PATH%" "%MIRROR_PATH%" /MIR /FFT /Z /W:5
echo ============================================